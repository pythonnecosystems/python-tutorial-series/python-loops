# Python 루프: For, While 및 Nested 루프

## <a name="intro"></a> 소개
이 포스팅에서는 반복과 Python에 네스팅을 위해 루프를 사용하고 사용하는 방법을 설명할 것이다. 또한 break, continue 및 else 문을 사용하여 루프의 흐름을 제어하는 방법도 다룰 것이다. 이 글이 끝날 때까지 반복과 조건을 포함하는 다양한 작업을 처리할 수 있는 효율적이고 우아한 코드를 작성할 수 있을 것이다.

루프(loops)는 프로그래밍에서 가장 기본적인 개념 중 하나이다. 특정 조건이나 미리 정의된 시퀀스에 따라 한 블록의 코드를 여러 번 실행할 수 있도록 해준다. 루프는 다음과 같은 작업을 수행하는 데 유용하다.

- 리스트, 튜플, 사전 또는 집합과 같은 컬렉션에서 데이터를 처리.
- 파일 또는 데이터베이스에서 데이터 읽기.
- 패턴 또는 공식을 기반으로 출력을 생성.
- 코드를 테스트하거나 디버깅.
- 게임이나 시뮬레이션을 생성.

Python은 for와 while 두 종류의 루프를 지원한다. for 루프는 리스트나 범위 객체 같은 일련의 항목에 걸쳐 반복되며 각 항목에 대하여 코드 블록을 실행한다. while 루프는 조건이 참인 한 코드 블록을 실행하고, 조건이 거짓이 되면 중지한다.

Python은 for와 while 루프 외에도 루프 안에 네스팅 루프를 둘 수도 있다. 이것은 루프 안에 루프를 둘 수도 있다는 것을 의미한다. 네스팅 루프는 행렬, 테이블, 그리드 또는 그래프와 같은 복잡한 패턴이나 구조를 만드는 데 유용하다.

이 포스팅에서는 Python에서 for 루프와 while 루프를 사용하는 방법, 그리고 루프를 네스팅하여 더 발전된 루프를 만드는 방법을 설명할 것이다. 또한 break, continue 및 else 문을 사용하여 루프의 동작을 제어하는 방법도 다룰 것이다. 데이터 분석, 텍스트 처리 및 그래픽 생성과 같은 다양한 작업에 루프를 사용하는 몇 가지 예를 볼 수 있을 것이다.

시작하기 전에 컴퓨터에 Python이 설치되어 있는지, Python의 기본 구문과 데이터 타입에 대해 잘 알고 있어야 한다. 복습이 필요하다면 [Python tutorial for beginners](https://levelup.gitconnected.com/%5E1%5E)을 읽어 보세요.

루프를 시작할 준비가 되었나요? Let's go!

## <a name="chap_2"></a> 루프는 무엇이며 왜 유용한가?
이 절에서 루프란 무엇이며, 왜 루프가 프로그래밍에 유용한지 알게 될 것이다. Python에서 foor 와 while 루프를 위한 기본 구문과 구조 또한 다룰 것이다.

루프는 특정 조건이나 미리 정의된 순서에 따라 코드 블록을 여러 번 반복하는 방법이다. 예를 들어, 루프를 사용하여 1부터 10까지의 숫자를 인쇄하거나 리스트의 요소를 따라 반복할 수 있다.

루프는 반복이 필요한 작업을 수행하는 데 유용하다. 예를 들어

- 리스트, 튜플, 사전 또는 집합과 같은 컬렉션에서 데이터 처리
- 파일 또는 데이터베이스에서 데이터 읽기.
- 패턴 또는 공식을 기반으로 출력 생성.
- 코드 테스트 또는 디버깅.
- 게임 또는 시뮬레이션 생성.

Python은 for와 while 두 종류의 루프를 지원한다. for 루프는 리스트나 범위 객체 같은 일련의 항목에 걸쳐 반복되며 각 항목에 대하여 코드 블록을 실행한다. while 루프는 조건이 참인 한 코드 블록을 실행하고, 조건이 거짓이 되면 중지한다.

Python에서 for 루프의 기본 구문과 구조는 다음과 같다.

```python
# A for loop in Python
for item in sequence:
    # Do something with item
```

Python에서 while 루프의 기본 구문과 구조는 다음과 같다.

```python
# A while loop in Python
while condition:
    # Do something while condition is true
```

두 경우 모두 반복되는 코드 블록을 루프 문 아래에 들여쓰기한다. 들여쓰기는 루프가 끝나는 위치를 Python에 알려주기 때문에 중요하다.

다음 섹션에서는 Python에서 for와 while 루프를 사용하는 방법과 break, continue 및 else 문을 사용하여 루프의 흐름을 제어하는 방법에 대한 몇 가지 예를 살펴본다. 또한 중첩(nested) 루프를 사용하여 더 복잡한 루프를 만드는 방법도 설명한다.

## <a name="chap_3"></a> Python에서 `for` 루프 사용법
이 섹션에서는 Python에서 루프를 사용하는 방법을 설명한다. 리스트, 튜플, 문자열, 사전 및 범위 객체와 같은 다양한 타입의 시퀀스를 반복하는 방법에 대한 예들을 볼 수 있다. 또한 enumerate 함수와 zip 함수를 사용하여 여러 시퀀스를 동시에 반복하는 방법도 다룬다.

Python의 for 루프는 리스트나 튜플 같은 항목의 시퀀스에서 반복하고 각 항목에 대해 코드 블록을 실행하는 방법이다. 예를 들어 for 루프를 사용하여 리스트의 요소를 출력할 수 있다.

```python
# A for loop to print the elements of a list
fruits = ["apple", "banana", "cherry"]
for fruit in fruits:
    print(fruit)
```

코드를 실행한 출력은 다음과 같다.

```
apple
banana
cherry
```

이 예에서는 리스트 `fruits`의 각 요소는 변수 `fruit`에 할당되고 각 요소에 대하여 `print` 함수가 실행된다. 시퀀스에 더 이상 요소가 없을 때 루프를 종료한다.

시퀀스의 현재 항목을 보유하는 변수의 이름은 유효한 Python 식별자라면 어떤 이름이라도 사용할 수 있다. 예를 들어 `fruit` 대신 `x`를 사용할 수 있다.

```python
# A for loop to print the elements of a list using x as the variable name
fruits = ["apple", "banana", "cherry"]
for x in fruits:
    print(x)
```

이 코드의 출력은 이전 코드와 동일하다.

for 루프를 사용하여 튜플, 문자열, 사전, 범위 객체 등 다른 타입의 시퀀스로 반복할 수도 있다. 예를 들어 for 루프를 사용하여 문자열의 문자를 인쇄할 수 있다.

```python
# A for loop to print the characters of a string
word = "Python"
for char in word:
    print(char)
```

이 코드의 출력은 다음과 같다.

```python
P
y
t
h
o
n
```

이 예에서는 문자열 `word`의 각 문자에 변수 `char`가 할당되고 각 문자에 대해 `print` 함수가 실행된다. 문자열에 더 이상 문자가 없을 때 루프가 종료된다.

for 루프를 사용하여 키-값 쌍의 모음인 사전을 반복할 수도 있다. 예를 들어 for 루프를 사용하여 사전의 키와 값을 인쇄할 수 있다.

```python
# A for loop to print the keys and values of a dictionary
person = {"name": "Alice", "age": 25, "city": "New York"}
for key, value in person.items():
    print(key, value)
```

이 코드의 출력은 다음과 같다.

```python
name Alice
age 25
city New York
```

이 예에서는 사전 `person`의 각 키-값 쌍에 키와 값 변수가 할당되고 각 쌍에 대해 `print` 함수가 실행된다. 사전에 더 이상 쌍이 없을 때 루프가 종료된다. 사전은 정렬되지 않은 컬렉션이므로 쌍의 순서는 다를 수 있다.

start, stop, step 값으로 지정할 수 있는 숫자 시퀀스인 범위 개체를 반복하기 위해 for 루프를 사용할 수도 있다. 예를 들어 for 루프를 사용하여 0부터 9까지의 숫자를 인쇄할 수 있다.

```python
# A for loop to print the numbers from 0 to 9
for num in range(10):
    print(num)
```

이 코드의 출력은 다음과 같다.

```python
0
1
2
3
4
5
6
7
8
9
```

이 예에서는 범위 객체의 각 숫자에 변수 num이 할당되고 각 숫자에 대해 `print` 함수가 실행된다. 숫자가 정지 값(이 경우 `10`)에 도달하면 루프가 종료된다. 정지 값은 범위에 포함되지 않으므로 마지막으로 인쇄되는 숫자는 `9`이다.

범위 객체에 대해 다른 시작과 단계 값을 지정할 수도 있다. 예를 들어 for 루프를 사용하여 `2`부터 `10`까지의 짝수를 인쇄할 수 있다.

```python
# A for loop to print the even numbers from 2 to 10
for num in range(2, 11, 2):
    print(num)
```

이 코드의 출력은 다음과 같다.

```python
2
4
6
8
10
```

이 예에서 시작 값은 `2`, 중지 값은 `11`, 단계 값은 `2`이다. 즉, 범위 개체는 숫자 `2`, `4`, `6`, `8`, `10`을 생성하고 숫자가 중지 값(이 경우 `11`)에 도달하면 루프가 종료된다. 정지 값은 범위에 포함되지 않으므로 마지막으로 출력되는 숫자는 `10`이다.

여러 시퀀스를 동시에 반복하고 각 시퀀스의 해당 요소를 액세스하고 싶을 때가 있다. 예를 들어 두 개의 리스트를 반복하여 해당 요소를 함께 인쇄하고자 한다. 둘 이상의 시퀀스 요소를 결합하는 반복자를 반환하는 zip 함수를 사용하여 이 작업을 수행할 수 있다. 예를 들어 for 루프와 zip 함수를 사용하여 세 사람의 이름과 나이를 인쇄할 수 있다.

```python
# A for loop and the zip function to print the names and ages of three people
names = ["Alice", "Bob", "Charlie"]
ages = [25, 30, 35]
for name, age in zip(names, ages):
    print(name, age)
```

이 코드의 출력은 다음과 같다.

```python
Alice 25
Bob 30
Charlie 35
```

이 예에서 `zip` 함수는 리스트 `names`와 `ages`의 요소를 결합하는 반복자를 반환하고, 각 요소 쌍에 `name`과 `age` 변수가 할당되고 각 쌍에 대해    `print` 함수가 실행된다. 반복기에 더 이상 쌍이 없을 때 루프가 종료된다. 가장 짧은 시퀀스가 소진되면 `print` 함수가 중지되므로 시퀀스의 길이가 다른 경우 일부 요소가 무시될 수 있다.

시퀀스를 반복하는 데 유용한 또 다른 함수는 enumerate 함수로, 시퀀스의 각 요소에 카운터를 더하는 반복기를 반환한다. 예를 들어 for 루프와 enumerate 함수를 사용하여 목록의 각 요소의 인덱스와 값을 인쇄할 수 있다.

```python
# A for loop and the enumerate function to print the index and value of each element of a list
fruits = ["apple", "banana", "cherry"]
for index, value in enumerate(fruits):
    print(index, value)
```

이 코드의 출력은 다음과 같다.

```python
0 apple
1 banana
2 cherry
```

이 예에서 `enuerate` 함수는 리스트 `fruits`의 각 요소에 카운터를 추가하는 반복자를 반환하고, 카운터와 요소의 각 쌍에 `index`와 `value`을 할당하고 각 쌍에 대해 `print` 함수를 실행한다. 반복기에 더 이상 쌍이 없을 때 루프가 종료된다. `enumerate` 함수는 기본적으로 카운터를 `0`부터 시작하지만 원하는 경우 다른 시작 값을 지정할 수 있다.

이 섹션에서는 Python에서 루프를 사용하는 방법을 배웠다. 리스트, 튜플, 문자열, 사전, 범위 객체 등 다양한 타입의 시퀀스를 반복하는 방법에 대한 예들을 살펴보았다. 또한 enumerate 함수와 zip 함수를 사용하여 여러 시퀀스를 동시에 반복하는 방법도 배웠다.

다음 섹션에서는 Python에서 while 루프를 사용하는 방법과 while 루프가 반복문과 어떻게 다른지 알아보겠다.

## <a name="chap_4"></a> Python에서 `while` 루프 사용법
이 섹션에서는 Python에서 while 루프를 사용하는 방법을 알아본다. 조건이 참인 동안 코드 블록을 실행하는 방법과 break, continue 및 else 문을 사용하여 루프를 종료하거나 수정하는 방법에 대한 예제들을 볼 수 있다. 또한 while 루프에서 무한 루프 및 기타 일반적인 오류를 피하는 방법도 다룰 것이다.

Python의 while 루프는 조건이 참인 동안 코드 블록을 실행하고 조건이 거짓이 되면 중지하는 방식이다. 예를 들어 while 루프를 사용하여 1부터 10까지의 숫자를 인쇄할 수 있다.

```python
# A while loop to print the numbers from 1 to 10
num = 1
while num <= 10:
    print(num)
    num = num + 1
```

이 코드의 출력은 다음과 같다.

```
1
2
3
4
5
6
7
8
9
10
```

이 예에서 변수 `num`은 `1`로 초기화되고, `while` 루프는 `num`이 `10`보다 작거나 같은지 확인한다. 조건이 참이면 루프는 코드 블록을 실행하여 `num` 값을 출력하고 `1`씩 증가시킵니다. 루프는 조건이 거짓이 될 때까지 이 과정을 반복하며, `num`이 `11`이 되었을 때 루프가 중지되고 프로그램은 다음 문으로 계속 진행된다.

부울 값(True 또는 False)으로 평가되는 모든 표현식을 while 루프의 조건으로 사용할 수 있다. 예를 들어 비교 연산자, 논리 연산자 또는 부울 값을 반환하는 함수를 사용할 수 있다. 예를 들어 while 루프를 사용하여 3으로 나눌 수 있는 1부터 10까지의 숫자를 인쇄할 수 있다.

```python
# A while loop to print the numbers from 1 to 10 that are divisible by 3
num = 1
while num <= 10:
    if num % 3 == 0:
        print(num)
    num = num + 1
```

이 코드의 출력은 다음과 같다.

```
3
6
9
```

이 예에서 while 루프는 `num`이 `10`보다 작거나 같은지 확인한다. 조건이 참이면 루프는 모듈로 연산자(%)를 사용하여 `num`이 `3`으로 나누어지는지 확인하는 코드 블록을 실행한다. 조건이 참이면 루프는 `num` 값을 출력한다. 그런 다음 루프는 `num`을 `1`씩 증가시키고 조건이 거짓이 될 때까지 이 과정을 반복한다(num이 `11`일 때 발생). 다음 루프가 중지되고 다음 문으로 프로그램을 계속 진행한다.

때로는 조건이 거짓이 되기 전에 루프를 종료하거나 수정하고 싶을 수도 있다. 예를 들어 특정 값에 도달하면 루프를 중지하거나 루프의 일부 반복을 건너뛰고 싶을 수 있다. 루프의 흐름을 제어하는 데 사용되는 break, continue 및 else 문을 사용하여 이 작업을 수행할 수 있다. 예를 들어 while 루프와 break 문을 사용하여 1부터 10까지의 숫자를 인쇄하고 숫자가 5가 되면 루프를 중지할 수 있다.

```python
# A while loop and the break statement to print the numbers from 1 to 10, and stop the loop when the number is 5
num = 1
while num <= 10:
    print(num)
    if num == 5:
        break
    num = num + 1
```

이 코드의 출력은 다음과 같다.

```
1
2
3
4
5
```

이 예에서 while 루프는 `num`이 `10`보다 작거나 같은지 확인한다. 조건이 참이면 루프는 `num` 값을 출력하는 코드 블록을 실행하고 `num`이 `5`와 같은지 확인한다. 조건이 참이면 루프는 루프를 종료하고 루프 뒤의 다음 문으로 이동하는 `break` 문을 실행한다. 그러면 루프가 중지되고 프로그램은 다음 문으로 계속 진행된다. break 문은 while 루프뿐만 아니라 모든 루프를 종료하는 데 사용할 수 있다.

또한 while 루프와 continue 문을 사용하여 루프의 일부 반복을 건너뛰고 다음 루프를 계속할 수도 있다. 예를 들어 while 루프와 continue 문을 사용하여 1부터 10까지의 홀수를 인쇄할 수 있다.

```python
# A while loop and the continue statement to print the odd numbers from 1 to 10
num = 1
while num <= 10:
    if num % 2 == 0:
        num = num + 1
        continue
    print(num)
    num = num + 1
```

이 코드의 출력은 다음과 같다.

```
1
3
5
7
9
```

이 예에서 while 루프는 `num`이 `10`보다 작거나 같은지 확인한다. 조건이 참이면 루프는 모듈로 연산자(%)를 사용하여 `num`이 짝수인지 확인하는 코드 블록을 실행한다. 조건이 참이면 루프는 `num`을 `1`씩 증가시키고 나머지 블록을 건너뛰고 루프의 다음 반복으로 이동하여 `continue` 문을 실행한다. 그런 다음 루프는 조건이 거짓이 될 때까지 이 과정을 반복하며 `num`이 `11`일 때 루프가 중지되고 프로그램이 다음 문으로 계속된다. `continue` 문은 while 루프뿐만 아니라 모든 루프의 반복을 건너뛰는 데 사용할 수 있다.

조건이 거짓이 될 때 while 루프와 else 문을 사용하여 코드 블록을 실행할 수도 있다. 예를 들어 while 루프와 else 문을 사용하여 1부터 10까지의 숫자를 인쇄하고 루프가 종료되면 메시지를 인쇄할 수 있다.

```python
# A while loop and the else statement to print the numbers from 1 to 10, and print a message when the loop ends
num = 1
while num <= 10:
    print(num)
    num = num + 1
else:
    print("The loop is over.")
```

이 코드의 출력은 다음과 같다.

```
1
2
3
4
5
6
7
8
9
10
The loop is over.
```

이 예제에서 while 루프는 `num`이 `10`보다 작거나 같은지 확인한다. 조건이 참이면 루프는 `num` 값을 출력하고 `1`씩 증가시키는 코드 블록을 실행한다. 루프는 조건이 거짓이 될 때까지 이 과정을 반복하며 `num`이 `11`일 때 다음 루프는 메시지를 출력하는 `else` 문을 실행한다. 그런 다음 루프가 중지되고 프로그램은 다음 문으로 계속 진행된다. `else` 문은 break 문 없이 루프가 정상적으로 종료되는 경우에만 실행된다는 점에 유의하세요.

while 루프에서 가장 흔하게 발생하는 오류 중 하나는 끝이 없는 루프인 무한 루프를 만드는 것이다. 이는 루프의 조건이 항상 참이거나 루프가 어떤 식으로든 조건을 수정하지 않는 경우 발생할 수 있다. 예를 들어, 다음 코드는 숫자 1을 영원히 출력하는 무한 루프를 생성한다.

```python
# An infinite loop that prints the number 1 forever
num = 1
while num == 1:
    print(num)
```

이 코드는 루프의 조건이 항상 참이고 루프가 `num`의 값을 변경하지 않기 때문에 무한 루프를 생성한다. 무한 루프를 방지하려면 항상 루프의 조건이 어느 시점에서 거짓이 될 수 있고 루프가 어떤 식으로든 조건을 수정하는지 확인해야 한다.

이 섹션에서는 Python에서 동안 루프를 사용하는 방법을 배웠다. 조건이 참인 동안 코드 블록을 실행하는 방법과 break, continue 및 else 문을 사용하여 루프를 종료하거나 수정하는 방법에 대한 예들을 살펴보았다. 또한 무한 루프와 while 루프의 일반적인 오류를 피하는 방법도 배웠다.

다음 섹션에서는 Python에서 중첩 루프를 사용하는 방법과 더 복잡한 루프를 만드는 방법을 배울 것이다.

## <a name="chap_5"></a> 루프에서 `break`, `continue` 및 `else` 문 사용법
이 섹션에서는 루프에서 break, continue 및 else 문을 사용하는 방법을 알아보겠다. 이러한 문을 사용하여 루프의 흐름을 제어하는 방법과 for 루프와 while 루프의 차이점에 대한 예제들을 볼 수 있다.

break, continue 및 else 문은 Python에서 루프의 동작을 수정하는 데 사용된다. 특정 조건에 따라 코드 블록을 종료하거나 건너뛰거나 실행할 수 있다. 이러한 문은 for 루프와 while 루프 모두에 사용할 수 있지만 작동 방식에는 약간의 차이가 있다.

break 문은 루프를 조기에 종료하고 루프 뒤의 다음 문으로 이동하는 데 사용된다. 예를 들어 특정 값에 도달하거나 오류가 발생하면 break 문을 사용하여 루프를 중지할 수 있다. 예를 들어 for 루프와 break 문을 사용하여 1부터 10까지의 숫자를 출력하고 숫자가 5가 되면 루프를 중지할 수 있다.

```python
# A for loop and a break statement to print the numbers from 1 to 10, and stop the loop when the number is 5
for num in range(1, 11):
    print(num)
    if num == 5:
        break
```

이 코드의 출력은 다음과 같다.

```
1
2
3
4
5
```

이 예에서 for 루프는 범위 객체를 반복하고 `num` 값을 출력한다. 또한 이 루프는 `num`이 `5`와 같은지 확인하고, 같으면 루프를 종료하고 루프 뒤의 다음 문으로 이동하는 `break` 문을 실행한다. 그러면 루프가 중지되고 프로그램은 다음 문을 계속 진행한다.

조건이 참인 동안 코드 블록을 실행하는 while 루프와 함께 break 문을 사용할 수도 있다. 예를 들어, while 루프와 break 문을 사용하여 1부터 10까지의 숫자를 출력하고 숫자가 5가 되면 루프를 중지할 수 있다.

```python
# A while loop and a break statement to print the numbers from 1 to 10, and stop the loop when the number is 5
num = 1
while True:
    print(num)
    if num == 5:
        break
    num = num + 1
```

이 코드의 출력은 이전 코드와 동일하다.

이 예에서 while 루프는 조건이 `True`이므로 `break` 문으로 중지되지 않는 한 영원히 실행된다. 루프는 `num` 값을 출력하고 `num`이 `5`와 같은지 확인한다. 만약 그렇다면 루프를 종료하고 루프 뒤의 다음 문으로 이동하는 `break` 문을 실행한다. 그러면 루프가 중지되고 프로그램은 다음 문으로 계속 진행된다.

break 문은 for 및 while 루프뿐만 아니라 모든 루프를 종료하는 데 사용할 수 있다. 예를 들어, 다른 루프 안에 있는 루프인 중첩 루프를 종료하는 데 break 문을 사용할 수 있다. 예를 들어 중첩 루프와 break 문을 사용하여 1부터 10까지의 곱셈표를 인쇄하고 곱이 50이 되면 루프를 중지할 수 있다.

```python
# A nested loop and a break statement to print the multiplication table from 1 to 10, and stop the loop when the product is 50
for i in range(1, 11):
    for j in range(1, 11):
        product = i * j
        print(i, "x", j, "=", product)
        if product == 50:
            break
```

이 코드의 출력은 다음과 같다.

```
1 x 1 = 1
1 x 2 = 2
1 x 3 = 3
1 x 4 = 4
1 x 5 = 5
1 x 6 = 6
1 x 7 = 7
1 x 8 = 8
1 x 9 = 9
1 x 10 = 10
2 x 1 = 2
2 x 2 = 4
2 x 3 = 6
2 x 4 = 8
2 x 5 = 10
2 x 6 = 12
2 x 7 = 14
2 x 8 = 16
2 x 9 = 18
2 x 10 = 20
3 x 1 = 3
3 x 2 = 6
3 x 3 = 9
3 x 4 = 12
3 x 5 = 15
3 x 6 = 18
3 x 7 = 21
3 x 8 = 24
3 x 9 = 27
3 x 10 = 30
4 x 1 = 4
4 x 2 = 8
4 x 3 = 12
4 x 4 = 16
4 x 5 = 20
4 x 6 = 24
4 x 7 = 28
4 x 8 = 32
4 x 9 = 36
4 x 10 = 40
5 x 1 = 5
5 x 2 = 10
5 x 3 = 15
5 x 4 = 20
5 x 5 = 25
5 x 6 = 30
5 x 7 = 35
5 x 8 = 40
5 x 9 = 45
5 x 10 = 50
6 x 1 = 6
6 x 2 = 12
6 x 3 = 18
6 x 4 = 24
6 x 5 = 30
6 x 6 = 36
6 x 7 = 42
6 x 8 = 48
6 x 9 = 54
6 x 10 = 60
7 x 1 = 7
7 x 2 = 14
7 x 3 = 21
7 x 4 = 28
7 x 5 = 35
7 x 6 = 42
7 x 7 = 49
7 x 8 = 56
7 x 9 = 63
7 x 10 = 70
8 x 1 = 8
8 x 2 = 16
8 x 3 = 24
8 x 4 = 32
8 x 5 = 40
8 x 6 = 48
8 x 7 = 56
8 x 8 = 64
8 x 9 = 72
8 x 10 = 80
9 x 1 = 9
9 x 2 = 18
9 x 3 = 27
9 x 4 = 36
9 x 5 = 45
9 x 6 = 54
9 x 7 = 63
9 x 8 = 72
9 x 9 = 81
9 x 10 = 90
10 x 1 = 10
10 x 2 = 20
10 x 3 = 30
10 x 4 = 40
10 x 5 = 50
```

## <a name="chap_6"></a> Python에서 nested 루프 사용법
이 섹션에서는 Python에서 중첩 루프를 사용하는 방법을 설명할 것이다. 다른 루프 안에 루프를 만드는 방법과 행렬, 표, 그리드 또는 그래프 작성과 같은 다양한 작업에 루프를 사용하는 방법에 대한 예제들을 볼 수 있다.

중첩 루프는 다른 루프 안에 배치되는 루프를 일컫는다. 즉, 외부 루프가 반복될 때마다 내부 루프가 완전히 다시 실행된다. 예를 들어 중첩 루프를 사용하여 1부터 10까지의 곱셈표를 인쇄할 수 있다.

```python
# A nested loop to print the multiplication table from 1 to 10
for i in range(1, 11):
    for j in range(1, 11):
        product = i * j
        print(i, "x", j, "=", product)
```

이 코드의 출력은 다음과 같다.

```
1 x 1 = 1
1 x 2 = 2
1 x 3 = 3
1 x 4 = 4
1 x 5 = 5
1 x 6 = 6
1 x 7 = 7
1 x 8 = 8
1 x 9 = 9
1 x 10 = 10
2 x 1 = 2
2 x 2 = 4
2 x 3 = 6
2 x 4 = 8
2 x 5 = 10
2 x 6 = 12
2 x 7 = 14
2 x 8 = 16
2 x 9 = 18
2 x 10 = 20
3 x 1 = 3
3 x 2 = 6
3 x 3 = 9
3 x 4 = 12
3 x 5 = 15
3 x 6 = 18
3 x 7 = 21
3 x 8 = 24
3 x 9 = 27
3 x 10 = 30
4 x 1 = 4
4 x 2 = 8
4 x 3 = 12
4 x 4 = 16
4 x 5 = 20
4 x 6 = 24
4 x 7 = 28
4 x 8 = 32
4 x 9 = 36
4 x 10 = 40
5 x 1 = 5
5 x 2 = 10
5 x 3 = 15
5 x 4 = 20
5 x 5 = 25
5 x 6 = 30
5 x 7 = 35
5 x 8 = 40
5 x 9 = 45
5 x 10 = 50
6 x 1 = 6
6 x 2 = 12
6 x 3 = 18
6 x 4 = 24
6 x 5 = 30
6 x 6 = 36
6 x 7 = 42
6 x 8 = 48
6 x 9 = 54
6 x 10 = 60
7 x 1 = 7
7 x 2 = 14
7 x 3 = 21
7 x 4 = 28
7 x 5 = 35
7 x 6 = 42
7 x 7 = 49
7 x 8 = 56
7 x 9 = 63
7 x 10 = 70
8 x 1 = 8
8 x 2 = 16
8 x 3 = 24
8 x 4 = 32
8 x 5 = 40
8 x 6 = 48
8 x 7 = 56
8 x 8 = 64
8 x 9 = 72
8 x 10 = 80
9 x 1 = 9
9 x 2 = 18
9 x 3 = 27
9 x 4 = 36
9 x 5 = 45
9 x 6 = 54
9 x 7 = 63
9 x 8 = 72
9 x 9 = 81
9 x 10 = 90
10 x 1 = 10
10 x 2 = 20
10 x 3 = 30
10 x 4 = 40
10 x 5 = 50
```

## <a name="chap_7"></a> 루프의 일반적인 오류와 함정을 피하는 방법
이 섹션에서는 루프의 일반적인 오류와 함정을 피하는 방법을 다룬다. 루프를 디버그하고 최적화하는 방법과 Python에서 루프를 작성할 때 모범 사례를 사용하는 방법에 대한 예제들을 살펴본다.

루프는 프로그래밍에서 가장 강력하고 유용한 기능 중 하나이지만 가장 까다롭고 오류가 발생하기 쉬운 기능 중 하나이기도 하다. 루프의 가장 일반적인 오류와 함정은 다음과 같다.

- 끝나지 않는 루프인 무한 루프 만들기.
- 루프에 잘못된 조건, 변수 또는 연산자를 사용하는 경우.
- 구문 오류 또는 논리 오류가 발생할 수 있는 코드 들여쓰기를 잘못하는 경우.
- 반복되는 시퀀스를 수정하여 예기치 않은 결과나 오류가 발생할 수 있는 경우.
- 반복 횟수를 너무 많이 또는 너무 적게 사용하여 루프의 성능이나 정확도에 영향을 줄 수 있다.

이러한 오류와 함정을 피하려면 항상 루프를 신중하게 테스트하고 디버그해야 하며, 더 나은 루프를 작성하는 데 도움이 되는 도구와 기법을 사용해야 한다. 이러한 도구와 기법 중 일부는 다음과 같다.

- 인쇄 문이나 주석을 사용하여 루프의 값과 흐름을 확인한다.
- 디버거 또는 IDE(통합 개발 환경)를 사용하여 루프를 단계별로 살펴보고 변수와 표현식을 검사한다.
- range, enumerate, zip 함수를 사용하여 쉽고 효율적으로 시퀀스를 만들고 반복한다.
- break, continue, else 문을 사용하여 루프의 흐름과 동작을 제어한다.
- 리스트 컴프리헨션 또는 생성기 표현식을 사용하여 간결하고 우아한 방식으로 루프에서 리스트 또는 iterator를 생성한다.
- itertools 모듈을 사용하여 일반적인 반복 작업을 위한 반복기를 만들고 조작한다.

이 섹션에서는 루프의 일반적인 오류와 함정을 피하기 위해 이러한 도구와 기법을 사용하는 방법에 대한 예들을 살펴본다. 또한 의미 있는 변수 이름 사용, PEP 8 스타일 가이드 따르기, 명확하고 간결한 코드 작성 등 Python에서 루프를 작성하기 위한 모범 사례들도 다룰 것이다.

끝이 없는 루프인 무한 루프를 피하는 방법의 예부터 시작해 보겠다. 무한 루프는 루프의 조건이 항상 참이거나 루프가 어떤 식으로든 조건을 수정하지 않는 경우 발생할 수 있다. 예를 들어 다음 코드는 숫자 1을 영원히 출력하는 무한 루프를 만든 코드이다.

```python
# An infinite loop that prints the number 1 forever
num = 1
while num == 1:
    print(num)
```

이 코드는 루프의 조건이 항상 참이고 루프가 `num`의 값을 변경하지 않기 때문에 무한 루프를 생성한다. 무한 루프를 방지하려면 항상 루프의 조건이 어느 시점에서 거짓이 될 수 있고 루프가 어떤 식으로든 조건을 수정하는지 확인해야 한다. 예를 들어, 각 반복에서 `num`을 `1`씩 증가시켜 위의 코드를 수정할 수 있다.

```python
# A fixed loop that prints the numbers from 1 to 10
num = 1
while num <= 10:
    print(num)
    num = num + 1
```

이 코드는 루프 조건을 `num <= 10`으로 변경하여 무한 루프를 수정했다. 즉, `num`이 `11`이 되면 루프가 중지된다. 또한 루프는 각 반복에서 `num`을 `1`씩 증가시키므로 결국 조건이 거짓이 된다. 이 코드의 출력은 다음과 같다.

```
1
2
3
4
5
6
7
8
9
10
```

무한 루프를 피하는 또 다른 예는 루프를 조기에 종료하고 루프 뒤의 다음 문으로 이동할 수 있는 `break` 문을 사용하는 것이다. 예를 들어 특정 값에 도달하거나 오류가 발생하면 `break` 문을 사용하여 루프를 중지할 수 있다. 예를 들어 while 루프와 break 문을 사용하여 1부터 10까지의 숫자를 출력하고 숫자가 5가 되면 루프를 중지할 수 있다.

```python
# A while loop and a break statement to print the numbers from 1 to 10, and stop the loop when the number is 5
num = 1
while True:
    print(num)
    if num == 5:
        break
    num = num + 1
```

이 코드는 루프를 종료하고 루프 뒤의 다음 문으로 이동하는 `break` 문을 사용하여 무한 루프를 방지한다. 루프는 `num`의 값을 출력하고 `num`이 `5`와 같은지 확인한다. 만약 그렇다면 루프를 중지하는 `break` 문을 실행한다. 그런 다음 루프는 `num`을 `1`씩 증가시키고 `break` 문이 실행될 때까지 프로세스를 반복한다. 이 코드의 출력은 다음과 같다.

```
1
2
3
4
5
```

다음 예에서는 논리 오류나 예기치 않은 결과를 초래할 수 있는 루프에서 잘못된 조건, 변수 또는 연산자를 사용하지 않는 방법을 살펴본다. 예를 들어 다음 코드는 1부터 10까지의 숫자를 인쇄하려고 하지만 루프 조건에 잘못된 연산자를 사용하고 있다.

```python
# A loop that uses the wrong operator in the condition
num = 1
while num = 10:
    print(num)
    num = num + 1
```

이 코드에서는 연산자 `=`가 비교가 아닌 할당에 사용되기 때문에 구문 오류가 발생한다. 두 값을 비교하려면 값이 같으면 참을 반환하고 그렇지 않으면 거짓을 반환하는 `==` 연산자를 사용해야 한다. 코드를 수정하려면 루프 조건에 `==` 연산자를 사용해야 한다.

```python
# A fixed loop that uses the correct operator in the condition
num = 1
while num == 10:
    print(num)
    num = num + 1
```

그러나 이 코드는 루프의 조건이 결코 참이 아니기 때문에 여전히 예상되는 출력을 생성하지 않는다. 루프는 `num`이 `10`과 같은지 확인하지만 `num`은 `1`로 초기화되어 루프에서 변경되지 않는다. 따라서 루프는 코드 블록을 실행하지 않으며 프로그램은 아무것도 출력하지 않고 종료된다. 코드를 수정하려면 루프에 다른 조건을 사용해야 한다(예: `num <= 10`). 즉, `num`이 `10`보다 클 때까지 루프가 실행된다.

```python
# A fixed loop that uses a different condition for the loop
num = 1
while num <= 10:
    print(num)
    num = num + 1
```

이 코드는 예상 출력을 다음과 같이 생성한다.

```python
1
    2
    3
    4
    5
    6
    7
    8
    9
    10
```

다음 예에서는 구문 오류 또는 논리 오류를 유발할 수 있는 코드 들여쓰기를 잘못하는 것을 방지하는 방법을 살펴본다. 예를 들어 다음 코드는 1부터 10까지의 숫자를 인쇄하려고 시도하지만 인쇄 문을 잘못 들여쓰고 있다.

```python
# A loop that indents the print statement incorrectly
num = 1
while num <= 10:
print(num)
    num = num + 1
```

이 코드는 `while` 문 아래에 인쇄 문이 들여쓰기가 되어 있지 않기 때문에 구문 오류가 발생한다. Python에서 들여쓰기는 인터프리터에게 루프에 속하는 코드 블록의 시작과 끝 위치를 알려주기 때문에 중요하다. 코드를 수정하려면 Python에서 권장하는 들여쓰기 양인 네 칸씩 인쇄 문을 들여쓰기해야 한다.

```python
# A loop that indents the print statement correctly
num = 1
while num <= 10:
    print(num)
    num = num + 1
```

이 코드는 이전 코드와 동일한 예상 출력을 생성한다.

그러나 코드를 잘못 들여쓰면 논리적 오류(프로그램 충돌을 일으키지는 않지만 부정확하거나 예상치 못한 결과를 생성하는 오류)가 발생할 수도 있다. 예를 들어 다음 코드는 1부터 10까지의 숫자를 인쇄하려고 시도하지만 `num = num + 1` 문을 잘못 들여쓰고 있다.

```python
# A loop that indents the num = num + 1 statement incorrectly
num = 1
while num <= 10:
    print(num)
num = num + 1
```

Python에서 **들여쓰기**는 코드 블록의 범위를 정의하는 데 사용된다. 들여쓰기는 코드 한 줄의 시작 부분에 있는 공백 또는 탭의 수를 나타낸다. Python에서 권장되는 들여쓰기의 양은 네 칸이다.

두 번째 코드 블록을 수정하려면 다음과 같이 `print` 문과 함께 반복적으로 실행되도록 `num = num + 1` 문을 네 칸 들여쓰기해야 한다.

```python
# A loop that correctly increments num
num = 1
while num <= 10:
    print(num)
    num = num + 1

```

이 코드 블록은 예상대로 `1`부터 `10`까지의 숫자를 인쇄한다.

들여쓰기가 잘못되면 논리적 오류(프로그램 충돌을 일으키지는 않지만 부정확하거나 예상치 못한 결과를 생성하는 오류)가 발생할 수 있다는 점에 유의해야 한다. 따라서 Python 코드를 작성할 때는 들여쓰기에 주의를 기울이는 것이 중요하다.

## <a name="conclusion"></a>맺으며
이 포스팅에서는 Python에서 루프를 사용하는 방법을 배웠다. 반복과 중첩을 위해 for와 while 루프를 사용하는 방법과 break, continue 및 else 문을 사용하여 루프의 흐름을 제어하는 방법을 살펴보았다. 또한 루프의 일반적인 오류와 함정을 피하는 방법과 Python에서 더 나은 루프를 작성하기 위해 도구와 기법을 사용하는 방법도 배웠다.

루프는 특정 조건이나 미리 정의된 순서에 따라 코드 블록을 여러 번 실행할 수 있기 때문에 프로그래밍에서 가장 기본적이고 강력한 개념 중 하나이다. 루프는 데이터 처리, 파일 읽기, 출력 생성, 코드 테스트, 게임 제작 등 반복이 필요한 작업을 수행하는 데 유용하다.

Python은 for와 while이라는 두 가지 타입의 루프를 지원한다. for 루프는 리스트이나 range 객체와 같은 일련의 항목을 반복하고 각 항목에 대한 코드 블록을 실행한다. while 루프는 조건이 참인 while 코드 블록을 실행하고 조건이 거짓이 되면 중지한다. 다른 루프 안에 루프를 중첩하여 더 복잡한 루프를 만들 수 있다.

break, continue 및 else 문을 사용하여 루프의 동작을 수정할 수 있다. break 문은 루프를 조기에 종료하고 루프 뒤의 다음 문으로 이동한다. continue 문은 루프의 현재 반복을 건너뛰고 다음 반복을 계속한다. else 문은 루프가 정상적으로 종료될 때 break 문 없이 코드 블록을 실행한다.

루프의 일반적인 오류와 함정을 피하려면 항상 루프를 신중하게 테스트하고 디버그해야 하며, 더 나은 루프를 작성하는 데 도움이 되는 도구와 기법을 사용해야 한다. 이러한 도구와 기법에는 print문, 주석, 디버거, IDE, range, enuerate, zip, 리스트 컴프리헨션, 제너레이터 표현식, itertools 등이 있다.

이 글을 통하여 Python에서 루프의 기본 사항과 다양한 작업에 루프를 사용하는 방법을 배웠다. 또한 의미 있는 변수 이름 사용하기, PEP 8 스타일 가이드 따르기, 명확하고 간결한 코드 작성하기 등 Python에서 루프를 작성하기 위한 몇 가지 모범 사례도 보였다.
