# Python 루프: For, While 및 Nested 루프 <sup>[1](#footnote_1)</sup>

## 목차

1. [소개](./python-loops.md#intro)
1. [루프는 무엇이며 왜 유용한가?](./python-loops.md#chap_2)
1. [Python에서 `for` 루프 사용법](./python-loops.md#chap_3)
1. [Python에서 `while` 루프 사용법](./python-loops.md#chap_4)
1. [루프에서 `break`, `continue` 및 `else` 문 사용법](./python-loops.md#chap_5)
1. [Python에서 nested 루프 사용법](./python-loops.md#chap_6)
1. [루프의 일반적인 오류와 함정을 피하는 방법](./python-loops.md#chap_7)
1. [맺으며](./python-loops.md#conclusion)


<a name="footnote_1">1</a>: [Python Tutorial 7 — Python Loops: For, While, and Nested Loops](https://python.plainenglish.io/python-tutorial-7-python-loops-for-while-and-nested-loops-a1c1306b6145)를 퍈역한 것이다.
